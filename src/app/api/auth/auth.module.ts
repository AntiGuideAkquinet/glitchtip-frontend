import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { MatLegacySnackBarModule as MatSnackBarModule } from "@angular/material/legacy-snack-bar";

@NgModule({
  declarations: [],
  imports: [CommonModule, MatSnackBarModule]
})
export class AuthModule {}
